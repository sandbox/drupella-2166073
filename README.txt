Picabulite
====================================
A multiplayer word drawing game for Drupal.
This is the lite version of http://drupella.com/product/picabulary-7

HOW TO PLAY
-----------
  - Guess the word the active player is drawing.
  - When it's your turn draw the given word for others to guess.
  - Both drawer and guesser gain points.


REQUIREMENTS
-----------
  - Flash plugin on client browser (96% of internet users have it installed)
  - Canvas support on client browser (All modern browsers; Firefox, Chrome, Safari, Opera, IE 9+ support canvas)


INSTALLATION
-----------
  1) Copy the module directory to sites/all/modules
  2) Enable the module in Drupal admin interface
  4) Obtain a free cirrus developer key from Adobe (http://labs.adobe.com/technologies/cirrus)
  5) Enter your cirrus rtmfp url at /admin/games/picabulite
  6) Give "play Picabulite" and "host Picabulite" permissions to your users at /admin/people/permissions
  7) Users can access the game at /picabulite path. A user must create a room and host the game for others to join.


EMBEDDING INTO A CUSTOM PAGE
-----------
  - Other than directly accessing the menu path /picabulite, you can embed the game into a page body in a few ways

  1) Iframe with Full HTML input format
      <iframe src="/picabulite" class="picabulite-iframe" frameborder="0" style="border: 1px solid #eee; width: 99%; height: 540px"></iframe>

  2) Popup link with Full HTML input format
      <a href="#" onclick="window.open('/picabulite', '', 'width=800,height=540,resizable=1'); return false;">Play Picabulite</a>