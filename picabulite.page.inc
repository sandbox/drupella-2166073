<?php

/**
 * @file
 * Implements game page callbacks.
 */

/**
 * Main handler for picabulite path.
 */
function picabulite() {
  $GLOBALS['devel_shutdown'] = FALSE;
  if (empty($_REQUEST['ajaxOp'])) {
    return picabulite_page();
  }
  return picabulite_ajax_op();
}

/**
 * Prints Picabulite page with the given page variables and exists.
 */
function picabulite_page($page = array()) {
  drupal_add_http_header('Content-Type', 'text/html; charset=utf-8');
  print theme('picabulite_page', array('page' => $page));
  drupal_page_footer();
  exit();
}

/**
 * Implements hook_preprocess_picabulite_page().
 */
function template_preprocess_picabulite_page(&$variables) {
  // Set default configuration values
  $page = &$variables['page'];
  $path = picabulite_script_path();
  $page['libUrl'] = base_path() . $path;
  $page['qs'] = $qs = '?' . variable_get('css_js_query_string', '0');
  // Css
  $page['cssUrls']['core'] = $page['libUrl'] . '/core/core.css' . $qs;
  $page['cssUrls']['picabulite'] = base_path() . drupal_get_path('module', 'picabulite') . '/picabulite.css' . $qs;
  // Js
  $page['jsUrls']['jquery'] = $page['libUrl'] . '/core/jquery.js';
  $page['jsUrls']['core'] = $page['libUrl'] . '/core/core.js' . $qs;
  // Conf
  if (!isset($page['scriptConf']['url'])) $page['scriptConf']['url'] = url($_GET['q']);
  if (!isset($page['title'])) $page['title'] = t('Picabulite');
  if (!isset($page['head'])) $page['head'] = '';
}

/**
 * Performs the ajax operation and returns json response.
 */
function picabulite_ajax_op() {
  if (function_exists($func = 'picabulite_ajax_op_' . $_REQUEST['ajaxOp'])) {
    // Always include ajaxOp in the response
    picabulite_add_ajax_response(array('ajaxOp' => $_REQUEST['ajaxOp']));
    $func();
  }
  $response = picabulite_add_ajax_response();
  $response['messages'] = drupal_get_messages();
  drupal_json_output($response);
  drupal_exit();
}

/**
 * Performs ajax op "load". TODO Include i18n js here and load it dynamically?
 */
function picabulite_ajax_op_load() {
  global $user;
  // Set library url
  $response['libUrl'] = base_path() . picabulite_script_path();
  // Add query string for dynamically loaded items
  $response['qs'] = '?' . variable_get('css_js_query_string', '0');
  // Set user details
  $response['user'] = array('uid' => $user->uid*1, 'name' => $user->uid ? $user->name : '');
  // Set session security key
  $response['securityKey'] = picabulite_get_security_key();
  $response['perms']['createRoom'] = user_access('host Picabulite');
  // Set RTMFP url
  if ($response['masterUrl'] = picabulite_var('master_url')) {
    // Add wordlists
    $response['wordlists'] = array(1 => 'General');
    // Default wordlist
    $response['defaultWordlist'] = 1;
    // Also add roomlist to the response
    picabulite_ajax_op_listrooms();
  }
  picabulite_add_ajax_response($response);
}

/**
 * Performs ajax op "listrooms".
 */
function picabulite_ajax_op_listrooms() {
  // Allow listing of the requested room even if it is hidden. List all for admins.
  $arg = user_access('administer Picabulite') ? 'all' : (empty($_POST['initRoom']) ? 0 : $_POST['initRoom'] * 1);
  picabulite_add_ajax_response(array('roomList' => picabulite_room_list($arg)));
}

/**
 * Performs ajax op "register".
 */
function picabulite_ajax_op_register() {
  global $user;
  // Check session security key
  if (!picabulite_check_security_key()) {
    // drupal_set_message(t('Invalid security key'), 'error');
    return FALSE;
  }
  // Already connected
  if ($peer = picabulite_load_posted_peer()) {
    // Check peer-user match
    if ($peer->uid != $user->uid) {
      return FALSE;
    }
    picabulite_add_ajax_response(array('peer' => $peer));
    return;
  }
  // Create peer
  $peer = new stdClass();
  if (empty($_POST['pid']) || !($peer->pid = picabulite_validate_pid($_POST['pid']))) {
    // drupal_set_message(t('Invalid peer id'), 'error');
    return FALSE;
  }
  $peer->uid = $user->uid*1;
  $peer->name = $user->uid ? $user->name : (variable_get('anonymous', t('Anonymous')) . mt_rand(10000, 99999));
  $peer->ip = picabulite_djb_hash(ip_address());
  $peer->connected = REQUEST_TIME;
  $peer->lastaction = REQUEST_TIME;
  if (!drupal_write_record('picabulite_peers', $peer)) {
    // drupal_set_message(t('Error creating the peer'), 'error');
    return FALSE;
  }
  picabulite_add_ajax_response(array('peer' => $peer));
}

/**
 * Performs ajax op "unregister".
 */
function picabulite_ajax_op_unregister() {
  global $user;
  // Check session security key
  if (!picabulite_check_security_key()) {
    return FALSE;
  }
  // Load peer
  if (!$peer = picabulite_load_posted_peer()) {
    return FALSE;
  }
  // Check peer-user match
  if ($peer->uid != $user->uid) {
    return FALSE;
  }
  // Not in a room
  if (empty($peer->rid)) {
    if (!picabulite_delete_peer($peer->pid)) {
      return FALSE;
    }
  }
  // Set as disconnected. It should be removed by the room owner when the leave signal is received.
  else {
    db_query("UPDATE {picabulite_peers} SET connected = 0 WHERE pid = :pid", array(':pid' => $peer->pid));
  }
  picabulite_add_ajax_response(array('pid' => $peer->pid));
}

/**
 * Performs ajax op "ping".
 */
function picabulite_ajax_op_ping() {
  global $user;
  // Check session security key
  if (!picabulite_check_security_key()) {
    return FALSE;
  }
  // Load peer
  if (!$peer = picabulite_load_posted_peer()) {
    return FALSE;
  }
  // Check peer-user match
  if ($peer->uid != $user->uid) {
    return FALSE;
  }
  // Update last action time
  db_query("UPDATE {picabulite_peers} SET lastaction = :time WHERE pid = :pid", array(':time' => REQUEST_TIME, ':pid' => $peer->pid));
  picabulite_add_ajax_response(array('pid' => $peer->pid));
}

/**
 * Performs ajax op "addroom".
 */
function picabulite_ajax_op_addroom() {
  global $user;
  // Check create access
  if (!user_access('host Picabulite')) {
    // drupal_set_message(t('Access denied'), 'error');
    return FALSE;
  }
  // Check session security key
  if (!picabulite_check_security_key()) {
    // drupal_set_message(t('Invalid security key'), 'error');
    return FALSE;
  }
  $room = new stdClass();
  // Validate title
  if (empty($_POST['title']) || !($room->title = picabulite_validate_varchar($_POST['title']))) {
    // drupal_set_message(t('Invalid title'), 'error');
    return FALSE;
  }
  // Load peer
  if (!$peer = picabulite_load_posted_peer(TRUE)) {
    return FALSE;
  }
  // Check peer-user match
  if ($peer->uid != $user->uid) {
    // drupal_set_message(t('Provided peer ID belongs to someone else'), 'error');
    return FALSE;
  }
  // Get random words
  if (!$words = picabulite_get_random_words(100)) {
    drupal_set_message(t('Wordlist is empty!'), 'error');
    return FALSE;
  }
  // Create the room
  $room->pid = $peer->pid;
  $room->rid = picabulite_djb_hash($room->pid);
  $room->uid = $user->uid*1;
  $room->pcount = 1;
  $room->pmax = 4;
  $room->pass = 0;
  $room->hidden = 0;
  $room->created = REQUEST_TIME;
  $room->secret = uniqid();
  $room->wlid = 1;
  if (!drupal_write_record('picabulite_rooms', $room)) {
    // drupal_set_message(t('Error creating the room'), 'error');
    return FALSE;
  }
  // Update peer room
  picabulite_update_peer_room($peer->pid, $room->rid);
  // Send room data
  $room->name = $peer->name;
  // Add words
  $room->words = picabulite_encode_words(implode("\n", $words));
  picabulite_add_ajax_response(array('room' => $room));
}

/**
 * Performs ajax op "delroom".
 */
function picabulite_ajax_op_delroom() {
  if (!$room = picabulite_validate_posted_room()) {
    return FALSE;
  }
  if (!picabulite_delete_room($room->rid)) {
    return FALSE;
  }
  db_query('UPDATE {picabulite_peers} SET rid = 0 WHERE rid = :rid', array(':rid' => $room->rid));
  picabulite_add_ajax_response(array('rid' => $room->rid*1));
}

/**
 * Performs ajax op "joinroom".
 */
function picabulite_ajax_op_joinroom() {
  if (!$room = picabulite_validate_posted_room()) {
    return FALSE;
  }
  if (!$peer = picabulite_load_posted_peer()) {
    return FALSE;
  }
  if ($peer->rid != $room->rid) {
    picabulite_update_peer_room($peer->pid, $room->rid);
    picabulite_update_room_pcount($room->rid, 1);
  }
  picabulite_add_ajax_response(array('pid' => $peer->pid, 'name' => $peer->name, 'uid' => $peer->uid*1, 'ip' => $peer->ip, 'rid' => $room->rid*1));
}

/**
 * Performs ajax op "leaveroom".
 */
function picabulite_ajax_op_leaveroom() {
  if (!$room = picabulite_validate_posted_room()) {
    return FALSE;
  }
  if (!$peer = picabulite_load_posted_peer()) {
    return FALSE;
  }
  if ($peer->rid == $room->rid) {
    picabulite_update_room_pcount($room->rid, -1);
    // Check if peer is still connected. The leave signal might be received after the disconnect signal.
    if ($peer->connected) {
      picabulite_update_peer_room($peer->pid, 0);
    }
    // Remove the peer if it is disconnected. This means it was still in this room when the disconnect signal is received.
    else {
      picabulite_delete_peer($peer->pid);
    }
  }
  picabulite_add_ajax_response(array('pid' => $peer->pid, 'rid' => $room->rid*1));
}

/**
 * Performs ajax op "unload".
 */
function picabulite_ajax_op_unload() {
  picabulite_ajax_op_delroom();
  picabulite_ajax_op_unregister();
}

/**
 * Loads a room by id.
 */
function picabulite_load_room($rid) {
  return db_query("SELECT * FROM {picabulite_rooms} WHERE rid = :rid", array(':rid' => $rid))->fetchObject();
}

/**
 * Deletes a room by id.
 */
function picabulite_delete_room($rid) {
  return db_query("DELETE FROM {picabulite_rooms} WHERE rid = :rid", array(':rid' => $rid));
}

/**
 * Returns the available rooms list.
 */
function picabulite_room_list($include_hidden_rid = 0) {
  $sql = 'SELECT r.*, p.name FROM {picabulite_rooms} r LEFT JOIN {picabulite_peers} p ON r.pid = p.pid ORDER BY r.created DESC';
  $result = db_query($sql);
  $rows = array();
  $include_all = $include_hidden_rid === 'all';
  $intfields = array('created', 'hidden', 'pcount', 'pmax', 'rid', 'uid', 'wlid');
  foreach ($result as $row) {
    if (!$row->hidden || $include_all || $row->rid == $include_hidden_rid) {
      $row->pass = (bool) $row->pass;
      foreach ($intfields as $field) {
        $row->$field *= 1;
      }
      unset($row->secret);
      $row->wordlist = 'General';
      $rows[$row->rid] = $row;
    }
  }
  return $rows;
}

/**
 * Loads a peer by id.
 */
function picabulite_load_peer($pid) {
  return db_query("SELECT * FROM {picabulite_peers} WHERE pid = :pid", array(':pid' => $pid))->fetchObject();
}

/**
 * Deletes a paaer by id.
 */
function picabulite_delete_peer($pid) {
  return db_query("DELETE FROM {picabulite_peers} WHERE pid = :pid", array(':pid' => $pid));
}

/**
 * Validates the posted room operation and returns the loaded room.
 */
function picabulite_validate_posted_room() {
  if (!$room = picabulite_load_posted_room()) {
    return FALSE;
  }
  if (!picabulite_check_room_secret($room)) {
    return FALSE;
  }
  return $room;
}

/**
 * Loads a room by posted id.
 */
function picabulite_load_posted_room($verbose = FALSE) {
  if (!empty($_POST['rid']) && $room = picabulite_load_room($_POST['rid'])) {
    return $room;
  }
  if ($verbose) {
    // drupal_set_message(t('Invalid room id'), 'error');
  }
  return FALSE;
}

/**
 * Validates the posted room secret.
 */
function picabulite_check_room_secret($room, $verbose = FALSE) {
  if (user_access('administer Picabulite') || !empty($_POST['secret']) && $_POST['secret'] === $room->secret) {
    return TRUE;
  }
  if ($verbose) {
    // drupal_set_message(t('Invalid room secret'), 'error');
  }
  return FALSE;
}

/**
 * Loads a peer by posted id.
 */
function picabulite_load_posted_peer($verbose = FALSE) {
  if (!empty($_POST['pid']) && $peer = picabulite_load_peer($_POST['pid'])) {
    return $peer;
  }
  if ($verbose) {
    // drupal_set_message(t('Invalid peer id'), 'error');
  }
  return FALSE;
}

/**
 * Update peer entering a room.
 */
function picabulite_update_peer_room($pid, $rid) {
  return db_query("UPDATE {picabulite_peers} SET rid = :rid, entered = :entertime, lastaction = :actiontime WHERE pid = :pid", array(':rid' => $rid, ':entertime' => REQUEST_TIME, ':actiontime' => REQUEST_TIME, ':pid' => $pid));
}

/**
 * Increase/decrease room peer count.
 */
function picabulite_update_room_pcount($rid, $add = 1) {
  return db_query("UPDATE {picabulite_rooms} SET pcount = pcount + :add WHERE rid = :rid", array(':add' => $add, ':rid' => $rid));
}

/**
 * Sets ajax response data.
 */
function picabulite_add_ajax_response($data = NULL) {
  static $response = array();
  if (isset($data) && is_array($data)) {
    picabulite_extend_conf($response, $data);
  } 
  return $response;
}

/**
 * Returns a session security key
 */
function picabulite_get_security_key() {
  return $GLOBALS['user']->uid ? drupal_get_token('picabulite') : 'anonymous';
}

/**
 * Validates the posted session security key
 */
function picabulite_check_security_key() {
  return isset($_POST['securityKey']) && $_POST['securityKey'] === picabulite_get_security_key();
}

/**
 * Returns Picabulite script path.
 */
function picabulite_script_path($subpath = NULL) {
  return 'sites/all/libraries/picabulite' . (isset($subpath) ? '/' . $subpath : '');
}

/**
 * Extend a configuration array with another.
 */
function picabulite_extend_conf(&$a, $b) {
  foreach ($b as $key => $val) {
    if (isset($a[$key])) {
      if (is_array($val) && is_array($a[$key])) {
        picabulite_extend_conf($a[$key], $val);
        continue;
      }
    }
    $a[$key] = $val;
  }
}

/**
 * Convert strings to integer using Bernstein's hash.
 */
function picabulite_djb_hash($str) {
  for ($i = 0, $h = 5381, $len = strlen($str); $i < $len; $i++) {
    // Since 32bit and 64bit differs we make sure the value is always 32 bit int. Converting while returning may fail.
    $h = (($h << 5) + $h + ord($str[$i])) & 0x7FFFFFFF;
  }
  return $h;
}

/**
 * Validate a peer id
 */
function picabulite_validate_pid($pid) {
  if (!is_string($pid)) return FALSE;
  $pid = trim($pid);
  $len = strlen($pid);
  if ($len != 64) return FALSE;
  if (preg_match('/[^a-zA-Z0-9]/', $pid)) return FALSE;
  return $pid;
}

/**
 * Validate a varchar
 */
function picabulite_validate_varchar($str, $min = 1, $max = 255) {
  if (!is_string($str)) return FALSE;
  $str = trim($str);
  $len = strlen($str);
  if ($len < $min || $len > $max) return FALSE;
  if (preg_match('/[^\x{0}-\x{FFFF}]/u', $str)) return FALSE;
  return $str;
}

/**
 * Validate an integer
 */
function picabulite_validate_int($num, $min = 1, $max = 0xFFFFFFFF) {
  if (!is_numeric($num)) return FALSE;
  $num *= 1;
  if (!is_int($num)) return FALSE;
  if ($num < $min || $num > $max) return FALSE;
  return $num;
}

/**
 * Returns random words from a wordlist
 */
function picabulite_get_random_words($limit = 100) {
  $words = array();
  $file = drupal_get_path('module', 'picabulite') . '/picabulite.wordlist.txt';
  if ($limit && $lines = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES)) {
    $words = array_rand(array_flip($lines), $limit);
    array_walk($words, 'trim');
  }
  return $words;
}

/**
 * Encode words that are sent to client
 * This will prevent the words to be seen directly.
 */
function picabulite_encode_words($str, $shift = 10) {
  $ret = '';
  $chars = preg_split('//u', $str, -1, PREG_SPLIT_NO_EMPTY);
  foreach ($chars as $char) {
    $ret .= $char === "\n" ? $char : picabulite_chr8(picabulite_ord8($char) + $shift);
  }
  return $ret;
}

/**
 * Utf8 compatible chr()
 */
function picabulite_chr8($code) {
  return mb_convert_encoding('&#' . $code . ';', 'UTF-8', 'HTML-ENTITIES');
}

/**
 * Utf8 compatible ord()
 */
function picabulite_ord8($chr) {
  $len = strlen($chr);
  if (!$len) return FALSE;
  $h = ord($chr{0});
  if ($h <= 0x7F) return $h;
  if ($h < 0xC2) return FALSE;
  if ($h <= 0xDF && $len > 1) return ($h & 0x1F) <<  6 | (ord($chr{1}) & 0x3F);
  if ($h <= 0xEF && $len > 2) return ($h & 0x0F) << 12 | (ord($chr{1}) & 0x3F) << 6 | (ord($chr{2}) & 0x3F);          
  if ($h <= 0xF4 && $len > 3) return ($h & 0x0F) << 18 | (ord($chr{1}) & 0x3F) << 12 | (ord($chr{2}) & 0x3F) << 6 | (ord($chr{3}) & 0x3F);
  return FALSE;
}