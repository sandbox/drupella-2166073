<?php

/**
 * @file
 * Serves administration pages.
 */

/**
 * Administaration page.
 */
function picabulite_admin_settings() {
  $output['conf'] = drupal_get_form('picabulite_admin_settings_form');
  $output['p'] = array('#markup' => '<p>&nbsp;</p>');
  $output['clear'] = drupal_get_form('picabulite_admin_clear_form');
  return $output;
}

/**
 * Settings form.
 */
function picabulite_admin_settings_form($form, &$form_state) {
  // Master url
  $form['picabulite_master_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Master Server URL'),
    '#default_value' => picabulite_var('master_url'),
    '#description' => t('Enter the URL of the RTMFP server that handles the peer connections. You can obtain it from <a href="!adobe-url">Adobe</a> for free. Example url: rtmfp://p2p.rtmfp.net/8a0ce537c8358cbb6ef40780-47feee722038/', array('!adobe-url' => 'http://labs.adobe.com/technologies/cirrus/')),
    '#required' => TRUE,
  );
  // Menu path
  $url = url('picabulite', array('absolute' => TRUE));
  $form['picabulite_url'] = array(
    '#markup' => '<p><strong>' . t('Game URL') . '</strong>: ' . l($url, $url) . '</p>',
  );

  $form = system_settings_form($form);
  return $form;
}

/**
 * Clear inactive form
 */
function picabulite_admin_clear_form($form, &$form_state) {
  $form['clear'] = array(
    '#type' => 'fieldset',
    '#title' => t('Clear inactive rooms and peers'),
    '#description' => t('Though they are automatically cleaned on every cache flush or cron run, you can use this button to manually clear dead rooms and peers.'),
  );
  $form['clear']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Clear'),
  );
  return $form;
}

/**
 * Clear inactive form submission
 */
function picabulite_admin_clear_form_submit($form, &$form_state) {
  picabulite_clear_inactive();
  drupal_set_message(t('Inactive rooms and peers have been cleared.'));
}